defmodule Young.MixProject do
  use Mix.Project

  def project do
    [
      app: :young,
      version: "0.1.0",
      elixir: "~> 1.12",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.html": :test
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Young.Application, []}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support", "test/mockery"]
  defp elixirc_paths(:dev), do: ["lib", "test/support", "test/mockery"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:meck, "~> 0.9.2", only: [:test], runtime: false},
      {:dialyxir, "~> 0.5", only: [:dev], runtime: false},
      {:excoveralls, "~> 0.10", only: [:test]}
    ]
  end
end
