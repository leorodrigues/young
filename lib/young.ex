defmodule Young do
  @moduledoc """
  Documentation for `Young`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Young.hello()
      :world

  """
  def hello do
    :world
  end
end
